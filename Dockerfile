FROM wyveo/nginx-php-fpm:php81
# Default WebRoot: /usr/share/nginx/html

# Root
COPY composer.lock composer.json composer.phar /usr/share/nginx
WORKDIR /usr/share/nginx
COPY . .

# Atualizando pacotes
RUN apt-get update && apt-get install  -o Dpkg::Options::="--force-confold" --allow-change-held-packages -y unzip zip php8.1-mbstring php8.1-zip php8.1-xml php8.1-dom php8.1-gd php8.1-soap curl

# Instalando Node18
RUN curl -L https://deb.nodesource.com/nsolid_setup_deb.sh | bash -s -- 18
RUN apt-get install -y nodejs

# Buildando vite
RUN npm install && npm run build

# Instalando pacotes
RUN php composer.phar install \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --prefer-dist


# Dados p/ uptime
# Apagando dados do uptime.dat
RUN rm -rf /usr/share/nginx/storage/app/uptime.dat
RUN echo 1 > /usr/share/nginx/storage/app/uptime.dat

# Dando permissão
RUN chown -R nginx:nginx storage bootstrap/cache

# Copiando a conf
COPY ./docker/nginx/default_nginx.conf /etc/nginx/conf.d/default.conf
