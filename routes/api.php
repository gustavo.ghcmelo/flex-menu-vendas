<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/client/{client}', [App\Http\Controllers\CardapioController::class, 'getInfoStore']);
    Route::get('/products/best-sellers', [App\Http\Controllers\CardapioController::class, 'getBestSellersProducts']);
    Route::get('/products/highlights', [App\Http\Controllers\CardapioController::class, 'getInfoHighlightProducts']);
    Route::get('/products/groups', [App\Http\Controllers\CardapioController::class, 'getGroupProducts']);
    Route::get('/service/schedules', [App\Http\Controllers\CardapioController::class, 'horarios']);
    Route::get('/service/status', [App\Http\Controllers\CardapioController::class, 'getServiceStatus']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
