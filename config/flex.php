<?php

return [
    'FLEX_VENDAS_API_URL' => env('FLEX_VENDAS_API_URL', 'http://localhost:8000'),
    'FLEX_API_TOKEN' => env('FLEX_API_TOKEN', null),
    'SSO_AUTH_URL' => env('SSO_AUTH_URL', 'http://localhost:8000')
];
