import { defineStore } from 'pinia'

export const useSearchStore = defineStore('search', {
    state: () => ({
        term: '',
    }),
    getters: {
        searchTerm: (state) => state.term,
    },
    actions: {
        updateStore (term) {
            this.term = term;
        }
    },
})
