import { defineStore } from 'pinia'

export const useGroupButtons = defineStore('groupButtons', {
    state: () => ({
        buttons: [],
    }),
    getters: {
        groupButtons: (state) => state.buttons,
    },
    actions: {
        updateGroupButtons (groups) {
            for(const group of groups) {
                if(!this.buttons.includes(group)) {
                    this.buttons.push(group)
                }
            }
        }
    },
})
