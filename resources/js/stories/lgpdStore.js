import { defineStore } from 'pinia'

export const useLgpdStore = defineStore('lgpd', {
    state: () => ({
        acceptTermsOfUse: localStorage.getItem('accept_terms_of_use') ? localStorage.getItem('accept_terms_of_use') : false,
    }),
    getters: {
        getAcceptTerms: (state) => state.acceptTermsOfUse,
    },
    actions: {
        updateAcceptTermsOfUse (response) {
            this.acceptTermsOfUse = response;
            localStorage.setItem('accept_terms_of_use', true);
        }
    },
})
