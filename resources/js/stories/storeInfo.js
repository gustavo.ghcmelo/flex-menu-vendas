import { defineStore } from 'pinia'

export const useStoreInfo = defineStore('storeInfo', {
    state: () => ({
        storeInfo: null,
    }),
    getters: {
        getStoreInfo: (state) => state.storeInfo,
    },
    actions: {
        updateStoreInfo (data) {
            this.storeInfo = data;
        }
    },
})
