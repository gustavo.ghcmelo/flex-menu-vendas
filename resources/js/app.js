import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/mdi-v6/mdi-v6.css";
import "quasar/src/css/index.sass";

import { createInertiaApp } from "@inertiajs/vue3";
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
import { Quasar, Notify, Dialog, Loading } from "quasar";
import quasarIconSet from "quasar/icon-set/svg-mdi-v6";
import VueSmoothScroll from 'vue3-smooth-scroll'
import { createPinia } from 'pinia'
import { createApp, h } from "vue";

const pinia = createPinia()

createInertiaApp({
    resolve: (name) =>
        resolvePageComponent(
            `./pages/${name}.vue`,
            import.meta.glob("./pages/**/*.vue"),
        ),

    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(VueSmoothScroll, { duration: 1000 })
            .use(plugin)
            .use(pinia)
            .use(Quasar, {
                plugins: {
                    Notify,
                    Dialog,
                    Loading
                }, // import Quasar plugins and add here
                iconSet: quasarIconSet,
                config: {
                    brand: {
                        primary: '#ffc107',
                        info: '#ff082b'
                    }
                }, // quasar config see: https://quasar.dev/start/vite-plugin/
            })
            .mount(el);
    },
});
