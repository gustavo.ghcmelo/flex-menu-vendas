// Components Pages
import Group_products from "./group_products.vue";
import Destaques from "./destaques.vue";
import Mais_vendidos from "./mais_vendidos.vue";
import Header from "./header.vue";
import SchedulesHours from "./schedulesHours.vue";
import SearchProducts from "./searchProducts.vue";

// Dialogs
import DetailsProduct from "./dialogs/detailsProduct.vue";
import LgpdDialog from './dialogs/lgpdDialog.vue';
import ContactFormSuccess from "./dialogs/contactFormSuccess.vue";
export {
    Group_products, Destaques, Mais_vendidos, Header, SchedulesHours, SearchProducts,

    DetailsProduct, LgpdDialog, ContactFormSuccess
}
