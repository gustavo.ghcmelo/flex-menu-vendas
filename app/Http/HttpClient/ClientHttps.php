<?php

namespace App\Http\HttpClient;

use GuzzleHttp as Client;

class ClientHttps {

    private Client\Client $connection;
    private String $baseUrl = '';

    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->connection = new Client\Client();
    }

    public function setBaseUrl(String $baseUrl): ClientHttps
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    public function builder(): Client\Client
    {
        return new Client\Client([
            'base_uri' => $this->baseUrl,
        ]);
    }
}
