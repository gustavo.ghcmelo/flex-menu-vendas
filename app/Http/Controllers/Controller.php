<?php

namespace App\Http\Controllers;

use App\Http\HttpClient\ClientHttps;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public static function connectionFlexVendasApi ()
    {
        $client = new ClientHttps();
        return $client->setBaseUrl(config('flex.FLEX_VENDAS_API_URL'))->builder();
    }

    public static function connectionSSOAuth ()
    {
        $client = new ClientHttps();
        return $client->setBaseUrl(config('flex.SSO_AUTH_URL'))->builder();
    }
}
