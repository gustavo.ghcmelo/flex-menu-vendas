<?php

namespace App\Http\Controllers;

use App\Traits\APIResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CardapioController extends Controller
{
    use APIResponse;
    public function getInfoStore (Request $request) {

        try {
            $client = explode('/', $request->path());
            $response = self::connectionFlexVendasApi()->request('GET', '/api/v1/cliente/' . $client[2], [
                'headers' => [
                    'filial_id' => 2,
                    'Authorization' => 'Bearer ' . config('flex.FLEX_API_TOKEN')
                ]
            ]);

            $data = json_decode($response->getBody(), true);
            $request->session()->put('filial_id', $data['codigo_filial']);
            return $this->successResponse($data);

        } catch (\Exception $e) {
            $code = is_int($e->getCode()) && $e->getCode() > 0 ? $e->getCode() : 500;
            return $this->errorResponse($e->getMessage(), $code);
        }
    }

    public function getBestSellersProducts (Request $request) {

        $response = self::connectionFlexVendasApi()->request('GET', 'api/v1/portal-maisvendidos', [
            'headers' => [
                'filial_id' => $request->session()->get('filial_id'),
                'Authorization' => 'Bearer ' . config('flex.FLEX_API_TOKEN')
            ]
        ]);
        $data = json_decode($response->getBody(), true);
        return $this->successResponse($data);
    }

    public function getInfoHighlightProducts (Request $request) {

        $response = self::connectionFlexVendasApi()->request('GET', 'api/v1/portal-destaques', [
            'headers' => [
                'filial_id' => $request->session()->get('filial_id'),
                'Authorization' => 'Bearer ' . config('flex.FLEX_API_TOKEN')
            ]
        ]);
        $data = json_decode($response->getBody(), true);
        return $this->successResponse($data);
    }

    public function getGroupProducts (Request $request) {

        $response = self::connectionFlexVendasApi()->request('GET', 'api/v1/portal-grupos-produtos', [
            'headers' => [
                'filial_id' => $request->session()->get('filial_id'),
                'Authorization' => 'Bearer ' . config('flex.FLEX_API_TOKEN')
            ]
        ]);
        $data = json_decode($response->getBody(), true);
        return $this->successResponse($data);
    }

    public function getServiceStatus () {
        if(!$this->get_config_service()){
            return $this->successResponse(false, 200);
        } else {
            return $this->successResponse($this->verifica_status_restaurant($this->horarios()), 200);
        }
    }

    public function horarios () {

        $response = self::connectionFlexVendasApi()->request('GET', 'api/v1/funcionamento', [
            'headers' => [
                'filial_id' => session()->get('filial_id'),
                'Authorization' => 'Bearer ' . config('flex.FLEX_API_TOKEN')
            ]
        ]);
        $data = json_decode($response->getBody(), true);
        return $this->formatter_horarios($data['data']);
    }


    public function verifica_status_restaurant($horarios){
        $day_of_week = date('N', strtotime(date('Y-m-d')));
        $hour_now = \Carbon\Carbon::now()->timezone('America/Recife');
        foreach($horarios as $key => $horario){
            if(is_array($horario[0])) {
                if($key == $day_of_week){
                    if(!isset($horario['status'])){
                        foreach($horario as $interval){
                            if($hour_now->between($interval['horario_inicial'], $interval['horario_final'])){
                                return true;
                            };
                        }
                    }
                }
            }
        }
        return false;
    }

    public function formatter_horarios($horarios){
        $nome_dias  = ["Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado", "Domingo"];
        $dias       = [ 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => [] ];

        foreach($horarios as $key => $horario){
            $dias[$horario['codigo_semana']][]= $horario;
        }

        foreach($dias as $key => $dia) {

            if(count($dia) == 0) {
                $dias[$key] = [(object)[ 'dia' => $nome_dias[$key - 1], 'status' => "Fechado" ]];
                continue;
            }

            usort($dia, function($a, $b){
                if($a['horario_inicial'] == $b['horario_inicial']){
                    return 0;
                }

                return $a['horario_inicial'] > $b['horario_inicial'] ? + 1 : -1;
            });

            $dias[$key] = $dia;
        }

        return $dias;
    }

    public function get_config_service(){

        $response = self::connectionFlexVendasApi()->request('POST', 'api/v1/consulta-configuracao', [
            'headers' => [
                'filial_id' => session()->get('filial_id'),
                'Authorization' => 'Bearer ' . config('flex.FLEX_API_TOKEN')
            ],
            'form_params' => [ 'nome_interno' => 'vendas_online' ]
        ]);

        $data = json_decode($response->getBody(), true);

        if($response->getStatusCode() != 200){
            return false;
        }

        if($data[1] != 200){
            Log::debug(json_encode($data));
            return false;
        }

        if($data[2] != "S"){
            return false;
        }

        return true;
    }
}
