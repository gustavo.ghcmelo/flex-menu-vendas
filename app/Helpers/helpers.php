<?php

use App\Enums\DocumentTypeEnum;
use App\Events\SaveOperation;
use App\Models\Document;
use App\Models\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

if (! function_exists('extractTokenFromHeader'))
{
    function extractTokenFromHeader($authorization): string
    {
        return explode(' ', $authorization)[1];
    }
}

if (! function_exists('createTransactionIdentifier'))
{
    function createTransactionIdentifier(String $documento_id, String $instituicao_id, String $user_id): string
    {
        $moment = time().mt_rand();
        $constUser = str_pad($user_id, 5, "0", STR_PAD_LEFT);
        $constDocument = str_pad($documento_id, 5, "0", STR_PAD_LEFT);
        $constInstitution = str_pad($instituicao_id, 5, "0", STR_PAD_LEFT);
        $transactionID = $constUser . $constDocument . $constInstitution . $moment;

        $Documento = Document::find($documento_id);
        $Documento->identificador_transacao = $transactionID;
        $Documento->save();

        return $transactionID;
    }
}

if (! function_exists('saveDocumentTransaction'))
{
    function saveDocumentTransaction(Request $request, $institutionID, $transactionID = 'tmp00000000000000000000000', $status = 'N'): int
    {
        $token = extractTokenFromHeader($request->header('authorization'));
        $sessao = Session::where('sessao', $token)->first();

        $request->merge(['sessao_id' => $sessao->id]);
        $request->merge(['identificador_transacao' => $transactionID]);
        $request->merge(['tipo_documento_id' => DocumentTypeEnum::PIX->value]);
        $request->merge(['instituicao_id' => $institutionID]);
        $request->merge(['status' => $status]);

        $document = Document::create($request->all());
        return $document->id;
    }
}

if (! function_exists('saveDocumentPixPaymentTransaction'))
{
    function saveDocumentPixPaymentTransaction(Request $request, $institutionID, $transactionID = 'payment_pix_00000000000000', $status = 'N'): int
    {
        $token = extractTokenFromHeader($request->header('authorization'));
        $sessao = Session::where('sessao', $token)->first();

        $request->merge(['sessao_id' => $sessao->id]);
        $request->merge(['identificador_transacao' => $transactionID]);
        $request->merge(['tipo_documento_id' => DocumentTypeEnum::PIX_PAGAMENTO->value]);
        $request->merge(['instituicao_id' => $institutionID]);
        $request->merge(['pagador_nome' => "{$request->user()->nome} {$request->user()->sobrenome}"]);
        $request->merge(['pagador_documento' => $request->user()->documento]);
        $request->merge(['data_vencimento' => \Carbon\Carbon::now()->format('Y-m-d')]);
        $request->merge(['status' => $status]);

        $document = Document::create($request->all());
        return $document->id;
    }
}

if (! function_exists('updateStatusDocument'))
{
    function updateStatusDocument(int $documento_id, String $status = 'N'): void
    {
        $Documento = Document::find($documento_id);
        $Documento->status = $status;
        $Documento->save();
    }
}

if (! function_exists('saveOperationLog'))
{
    function saveOperationLog($token, $requisicao, $observacao)
    {
        $sessao_id = 0;
        $sessao = Session::where('sessao', $token)->first();

        if (!$sessao) {
            Log::debug(['LOG DE OPERACAO' => "SESSÃO NÃO IDENTIFICADA PARA O TOKEN $token"]);
        } else {
            $sessao_id= $sessao->id;
        }

        event(new SaveOperation($sessao_id, $requisicao, $observacao));
    }
}
