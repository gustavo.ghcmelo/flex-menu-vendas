<?php

namespace App\Traits;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

trait APIResponse
{
    /**
     * Building success response
     * @param $data
     * @param int $code
     */
    public function successResponse($data, $statusCode = Response::HTTP_OK)
    {
        return response()->json([
            'error' => false,
            'status' => $statusCode,
            'data' => $data
        ], Response::HTTP_OK);
    }

    /**
     * Building success response
     * @param $data
     * @param int $code
     */
    public function errorResponse($reason, $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        Log::debug(["ERROR: {$statusCode}" => $reason]);
        return response()->json([
            'error' => true,
            'status' => $statusCode,
            'message' => $reason
        ], $statusCode);
    }
}
